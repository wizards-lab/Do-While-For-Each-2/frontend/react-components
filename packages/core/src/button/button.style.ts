import {createUseStyles} from 'react-jss'
import * as Classes from '../classes';
import {outlineColor} from '../colors';
import {borderRadius, fontSize, fontSizeLarge, fontSizeSmall, Intent, isSizeStd, Size} from '../variables';
import {boxShadow, boxShadowActive, buttonHeight, buttonHeightLarge, buttonHeightSmall, color, colorDisabled, gradient, iconSpacing, iconSpacingLarge, iconSpacingSmall, intents, padding, paddingLarge, paddingSmall} from './variables';

export class ButtonStyle {
  static useStyles = createUseStyles({
    button: {
      display: 'inline-flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',

      border: 'none',
      borderRadius,

      textAlign: 'left',
      verticalAlign: 'middle',

      cursor: 'default',

      '&:focus, &:active': {
        outline: {color: outlineColor, width: 2, style: 'solid'},
        outlineOffset: 2
      },
      '&:disabled': {
        backgroundImage: 'none !important',
        boxShadow: 'none !important',
        cursor: 'not-allowed !important',
        outline: 'none !important',
        userSelect: 'none'
      },
      '& > :last-child, &:empty::before': {
        marginRight: 0,
      },

      ...ButtonStyle.sizes(),
      ...ButtonStyle.intents(),
    }
  })

  static sizes() {
    return Object.values(Size).reduce((acc, size: Size) => {
      let height = buttonHeight
      let Padding = padding
      let FontSize = fontSize
      let marginRight = iconSpacing
      switch (size) {
        case Size.SMALL:
          height = buttonHeightSmall
          Padding = paddingSmall
          FontSize = fontSizeSmall
          marginRight = iconSpacingSmall
          break
        case Size.LARGE:
          height = buttonHeightLarge
          Padding = paddingLarge
          FontSize = fontSizeLarge
          marginRight = iconSpacingLarge
          break
      }
      acc[isSizeStd(size)
        ? '&'
        : `&.${size}, .${size} &`
        ] = {
        minWidth: height,
        minHeight: height,
        padding: Padding,
        fontSize: FontSize,
      }
      acc[isSizeStd(size)
        ? '& > *, &::before'
        : `&.${size} > *, &.${size}::before, .${size} & > *, .${size} &::before`
        ] = {
        marginRight
      }
      return acc
    }, {})
  }

  static intents() {
    return Array.from(intents.entries()).reduce(
      (acc, [intent, [defaultColor, hoverColor, activeColor, disabledColor]]) => {
        const isStd = intent === Intent.STANDARD
        acc[isStd ? '&:not([class*="intent-"])' : `&.${Classes.intentClass(intent)}`] = {
          backgroundColor: defaultColor,
          backgroundImage: gradient(intent),
          boxShadow: boxShadow(intent),
          color: color(intent),
          '&:hover': {
            backgroundColor: hoverColor,
            boxShadow: boxShadow(intent),
          },
          [`&:active, &.${Classes.ACTIVE}`]: {
            backgroundColor: activeColor,
            backgroundImage: 'none',
            boxShadow: boxShadowActive(intent),
          },
          '&:disabled': {
            backgroundColor: disabledColor,
            color: colorDisabled(intent),
          }
        }
        return acc
      }, {})
  }
}
