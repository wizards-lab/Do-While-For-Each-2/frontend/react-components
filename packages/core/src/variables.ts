export const gridSize = 10

export const fontSize = gridSize * 1.4
export const fontSizeLarge = gridSize * 1.6
export const fontSizeSmall = gridSize * 1.2

// Icon variables

export const iconSizeStandard = 16
export const iconSizeSmall = iconSizeStandard - 4
export const iconSizeLarge = iconSizeStandard + 4

// Grids & dimensions

export const borderRadius = Math.floor(gridSize / 3)

export enum Size {
  STANDARD = 'standard',
  SMALL = 'small',
  LARGE = 'large',
}

export enum Intent {
  STANDARD = 'standard',
  PRIMARY = 'primary',
  SUCCESS = 'success',
  WARNING = 'warning',
  DANGER = 'danger'
}

export const isSizeStd = (size: Size) => size === Size.STANDARD
export const isIntentStd = (intent: Intent) => intent === Intent.STANDARD
